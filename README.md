# Mndl
Draw mandala like images and save them in your device.

You can choose between two shapes:
* square
* circle

and three number of divider lines:
* 4
* 8
* 16
 
If you want to get only the HTML files, you can find them in the [assets](/app/src/main/assets) folder

## Screenshots
<img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/0.png">
<img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/1.png">
<img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/2.png">
